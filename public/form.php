<html>
  <head>
  <style type="text/css" media="all">
  @import url("style.css");
  </style>
  </head>
  <body>

<?php
  if (!empty($_COOKIE['name1_error'])||!empty($_COOKIE['email_error'])||!empty($_COOKIE['year_error'])||!empty($_COOKIE['biography_error']) {
    print '<div style="text-align:center;"><a style="color:red;margin:0 auto;">*</a><a>Это обязательные поля</a></div>';
  }
  else{
    print '<div style="text-align:center;"><a>Спасибо, результаты сохранены</a></div>';
  }
  }

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<div class="container">  
    <h2><strong>Форма</strong></h2>
    
    <a id="form"></a>
    <form action="index1.php" method="POST">
        
        <label>
            Имя:<br/>
            <input name="name1" 
            <?php if ($errors['name1']) {print 'class="error"';} ?>  type="text"
            value="<?php print $values['name1']; ?>" />
        </label><br/><br/>

        <label>
            Email:<br/>
            <input name="e_mail" 
            <?php if ($errors['email']) {print 'class="error"';} ?>
            value="<?php print $values['email']; ?>" type="email" />
        </label><br/><br/>

        <label>
            Дата рождения:<br/>
            <input name="year" 
            <?php if ($errors['year']) {print 'class="error"';} ?>
            value="<?php print $values['year']; ?>" type="date" />
        </label><br/><br/>

        Пол:<br/>
        <label><input type="radio" checked="checked"
            name="gender" value="m" />
            Муж</label>
        <label><input type="radio"
            name="gender" value="f" />
            Жен</label><br/><br/>

        Количество конечностей:<br/>
        <label><input type="radio" checked="checked"
            name="limbs" value="4" />
            4</label>
        <label><input type="radio" checked="checked"
            name="limbs" value="3" />
            3</label>
        <label><input type="radio" checked="checked"
            name="limbs" value="2" />
            2</label>
        <label><input type="radio" checked="checked"
            name="limbs" value="1" />
            1</label>
        <label><input type="radio"
            name="limbs" value="0" />
            0</label><br/><br/>

        <label>
            Сверхспособности:
            <br/>
            <select name="super_skill"
            multiple="multiple">
            <option value="god">Бессмертие</option>
            <option value="fireball" selected="selected">Огненные шары</option>
            <option value="fly" selected="selected">Полет</option>
            </select>
        </label><br/><br/>

        <label>
            Биография:<br/>
            <textarea name="biography"
            <?php if ($errors['biography']) {print 'class="error"';} ?>
            <?php print $values['biography']; ?>
            ></textarea>
        </label><br/><br/>

        <br/>
        <label><input type="checkbox" checked="checked"
            name="check-1" />
            С контрактом ознакомлен</label><br/><br/>

        <input type="submit" value="Отправить" />

        

        </form>
    </div>
  </body>
</html>